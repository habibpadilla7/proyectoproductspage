import { Component } from '@angular/core';
import { DataApiService } from "./services/data-api.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pagina';
  constructor(private dataApi:DataApiService) { }

  ngOnInit() {
    //this.getListBooks();
  }

  getListBooks(){
    this.dataApi.getAllBooks().subscribe(photos => console.log(photos));
  }
}
