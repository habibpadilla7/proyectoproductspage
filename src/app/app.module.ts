import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';

//ROUTING
import { APP_ROUTING } from './app.routes';


//Component
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { ProductsComponent } from './component/products/products.component';
import { MenuhComponent } from './component/menuh/menuh.component';
import { SearchComponent } from './component/search/search.component';
import { ImgcarouselComponent } from './component/imgcarousel/imgcarousel.component';

//services
import { DataApiService } from "./services/data-api.service";
import { ProductService } from './services/product.service';
import { MenuleftComponent } from './component/menuleft/menuleft.component';
import { JumbotronComponent } from './component/jumbotron/jumbotron.component';
import { ContactComponent } from './component/contact/contact.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    MenuhComponent,
    SearchComponent,
    ImgcarouselComponent,
    MenuleftComponent,
    JumbotronComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    APP_ROUTING
  ],
  providers: [
    DataApiService,ProductService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
