import { Component, OnInit } from '@angular/core';
import { ProductService, Product } from '../../services/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {
  private product:Product[];
  constructor(private productService:ProductService) {
  }

  ngOnInit() {
    this.productService.getAllProducts().subscribe(photos=>this.product=photos);

    //this.getListProduct();
    //this.products =this.productService.getAllProducts();
    //console.log(this.products);

  //  this.productService.getAllProduct().subscribe(photos=>console.log(photos));
    //     console.log(todos);
    // });
  }
  //getListProducts:product[](){
  //  this.productService.getAllProducts().subscribe(photos => photos);
  //}


  //getListProduct(){
  //  this.productService.getAllProduct().subscribe(photos=>console.log(photos));
  //}

}
