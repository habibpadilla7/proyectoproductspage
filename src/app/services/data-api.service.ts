import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private http:HttpClient) { }

  getAllBooks(){
    const url_api='https://jsonplaceholder.typicode.com/photos';
    return this.http.get(url_api);
  }
}
