
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/internal/observable';

@Injectable({
  providedIn: 'root'
})
export class ProductService{

  constructor(private http:HttpClient){
    console.log("servicio listo para usar!!!");
  }
  getAllProducts(){
	   const path='https://jsonplaceholder.typicode.com/photos';
	    return this.http.get<Product[]>(path);
  }
  //getProduct(){
    //return this.products;
  //}
  //buscarHeroes(texto:string){
    //let productArr:Product[] = [];
  //  texto= texto.toLowerCase();
    //for(let product of this.products){
    //  let nombre = product.nombre.toLowerCase();
      //if(nombre.indexOf(texto) >= 0){
        //productArr.push(product);
    //  }
    //}
  //  return productArr;
  //}
}
export interface Product{
  albumId:string;
  id:string;
  title:string;
  url:string;
  thumbnailUrl:string
};
